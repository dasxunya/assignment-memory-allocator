#ifndef _TESTS_H_
#define _TESTS_H_
#include "mem.h"
#include "mem_internals.h"

void t1(struct block_header *first_block);
void t2(struct block_header *first_block);
void t3(struct block_header *first_block);
void t4(struct block_header *first_block);
void t5(struct block_header *first_block);
void starter(); //to run tests

#endif


