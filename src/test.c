#define _DEFAULT_SOURCE
#include "mem_debug.h"
#include "mem.h"
#include "test.h"
#include "util.h"

#define HEAP_SIZE 10000


static struct block_header* data_block_preparation(void* data) {
    return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}

void t1(struct block_header *first_block) {
    debug("Единожды выделим и очистим память:\n");
    void *prepared_data = _malloc(HEAP_SIZE - 4 * 70);
    if (prepared_data == NULL)  {err("Error: Результат вызова malloc - NULL\n");}
    debug_heap(stdout, first_block);
    if (first_block->is_free != false){err("Error: Ошибка при освобождении блока\n");}
    if(first_block->capacity.bytes != HEAP_SIZE - 4 * 70){err("Error: Ошибка в размере блока!\n");}
    _free(prepared_data);
    debug("Первый тест отработал.\n\n");
}


void t2(struct block_header *first_block) {
    debug("Выделим память два раза и опустошим один блок:\n");
    void *prepared_data1 = _malloc(HEAP_SIZE - 7952);
    void *prepared_data2 = _malloc(2000);
    if (prepared_data1 == NULL || prepared_data2 == NULL){
        err("Error: Результат вызова malloc - NULL\n");
    }
    _free(prepared_data1);
    debug_heap(stdout, first_block);
    struct block_header* block1 = data_block_preparation(prepared_data1);
    struct block_header* block2 = data_block_preparation(prepared_data2);
    if (!block1->is_free){
        err("Error: Первый блок не пустой.\n");
    }
    if (block2->is_free){
        err("Error: Второй блок пустой.\n");
    }
    _free(prepared_data2);
    debug("Второй тест отработал.\n\n");
}


void t3(struct block_header *first_block) {
    debug("Опустошим два блока из 3-ы выделенной памяти:\n");
    void *prepared_data1 = _malloc(HEAP_SIZE - 7952);
    void *prepared_data2 = _malloc(HEAP_SIZE - 6928);
    void *prepared_data3 = _malloc(HEAP_SIZE - 5904);

    if (prepared_data1 == NULL || prepared_data2 == NULL || prepared_data3 == NULL){err("Error: Один из блоков пустой.\n");}

    debug_heap(stdout, first_block);
    _free(prepared_data2);
    _free(prepared_data1);

    struct block_header* block1 = data_block_preparation(prepared_data1);
    struct block_header* block3 = data_block_preparation(prepared_data3);

    if (!block1->is_free){err("Error: Первый блок не пустой.\n");}
    if (block3->is_free){err("Error: Третий блок пустой.\n");}
    if (block1->capacity.bytes != (HEAP_SIZE - 7952) + (HEAP_SIZE - 6928) + offsetof(struct block_header, contents)){err("Размер первого блока не совпадает.\n");}

    _free(prepared_data1);
    _free(prepared_data2);
    _free(prepared_data3);
    debug("Третий тест отработал.\n\n");
}

void t4(struct block_header *first_block) {
    debug("Трижды выделим память и попробуем увеличить размер старого региона с помощью нового:\n");
    void *prepared_data1 = _malloc(HEAP_SIZE);
    void *prepared_data2 = _malloc(HEAP_SIZE);
    void *prepared_data3 = _malloc(HEAP_SIZE/2);
    if (prepared_data1 == NULL || prepared_data2 == NULL || prepared_data3 == NULL){
        err("Error: Результат вызова malloc - NULL\n");
    }
    _free(prepared_data2);
    debug_heap(stdout, first_block);

    struct block_header* block1 = data_block_preparation(prepared_data1);
    struct block_header* block2 = data_block_preparation(prepared_data2);
    if ((uint8_t*) block1->contents + block1->capacity.bytes != (uint8_t*) block2){err("Error: Проблема с адресом второго блока.\n");}
    _free(prepared_data1);
    _free(prepared_data3);
    debug("Четвертый тест отработал.\n\n");
}

void t5(struct block_header *first_block) {
    debug("Проверка на выделение памяти для региона в другом месте из-за недостатка места в куче\n");
    void *prepared_data1 = _malloc(10000*4);
    if (prepared_data1 == NULL) {
        err("Error: Результат вызова malloc - NULL\n");
    }
    struct block_header *beginning = first_block;
    while (beginning->next != NULL) {
        beginning = beginning->next;
    }

    map_pages((uint8_t *) beginning + size_from_capacity(beginning->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void *prepared_data2 = _malloc(HEAP_SIZE*20);
    debug_heap(stdout, first_block);
    struct block_header *prepared_data2_block = data_block_preparation(prepared_data2);
    if (prepared_data2_block == beginning) {
        err("Error: Блок оказался около первой кучи.\n");
    }
    _free(prepared_data1);
    _free(prepared_data2);
    debug("Пятый тест отработал.\n\n");
}


void starter() {
    debug("Инициализация кучи (HEAP_SIZE = 10000)\n");
    void *heap = heap_init(HEAP_SIZE);
    if (heap == NULL){err("Не удалось проинициализировать\n");}

    debug("Прохождение тестов:\n");

    debug("Результат первого теста:\n");
    t1(heap);

    debug("Результат второго теста:\n");
    t2(heap);

    debug("Результат третьего теста:\n");
    t3(heap);

    debug("Результат четвертого теста:\n");
    t4(heap);

    debug("Результат пятого теста:\n");
    t5(heap);
    
    debug("Тесты успешно пройдены\n");
}
